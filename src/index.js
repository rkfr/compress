import {
    ifDistFolderExist,
    defaultComress,
    createDistFolder,
} from "./functions";

ifDistFolderExist()
    .then(isExists => isExists ? defaultComress() : createDistFolder())
    .then(() => defaultComress())
    .catch(err => { throw new Error(`Error: ${err}`) });
