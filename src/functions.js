import fs from "fs";
import config from "./config";
import compressImages from "compress-images";

const sourceFolder = () => `${config.sourceFolderName}/*.{${config.supportedFormats.toString()}}`;

const distFolder = () => config.compressedFolderName + '/';

const ifDistFolderExist = () => new Promise(res => fs.access(distFolder(), (err) => res(!err)));

const createDistFolder = () => new Promise((res, rej) => fs.mkdir(distFolder(), err => !err ? res() : rej(err)));

const defaultComress = () => {
    compressImages(
        sourceFolder(),
        distFolder(),
        { compress_force: false, statistic: true, autoupdate: true }, false,
        { jpg: { engine: "mozjpeg", command: ["-quality", "60"] } },
        { png: { engine: "pngquant", command: ["--quality=20-50", "-o"] } },
        { svg: { engine: "svgo", command: "--multipass" } },
        {gif: {engine: 'gifsicle', command: ['--colors', '64', '--use-col=web']}},
        () => {},
    );
};

export {
    ifDistFolderExist,
    defaultComress,
    createDistFolder,
};
