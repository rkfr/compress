export default {
    sourceFolderName: 'target',
    compressedFolderName: 'dist',
    supportedFormats: ['jpg', 'png', 'svg', 'gif'],
};
